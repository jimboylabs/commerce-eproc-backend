-- migrate:up
CREATE TABLE categories(
  id uuid NOT NULL PRIMARY KEY,
  name VARCHAR(255) NOT NULL UNIQUE,
  created_at TIMESTAMPTZ NOT NULL DEFAULT now(),
	updated_at TIMESTAMPTZ NOT NULL DEFAULT now() 
);

-- migrate:down
DROP TABLE IF EXISTS categories;
