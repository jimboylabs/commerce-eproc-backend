-- migrate:up
CREATE TABLE reviews(
  id uuid NOT NULL PRIMARY KEY,
  product_id UUID CONSTRAINT product_id_fkey REFERENCES products(id) ON DELETE SET NULL,
  message VARCHAR(255),
  rating INT,
  created_at TIMESTAMPTZ NOT NULL DEFAULT now(),
	updated_at TIMESTAMPTZ NOT NULL DEFAULT now() 
);

-- migrate:down
DROP TABLE IF EXISTS reviews;
