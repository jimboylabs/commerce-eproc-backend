-- migrate:up
CREATE TABLE product_images(
  id uuid NOT NULL PRIMARY KEY,
  product_id UUID CONSTRAINT product_id_fkey REFERENCES products(id) ON DELETE SET NULL,
  image_url VARCHAR(255) NOT NULL,
  short_desc VARCHAR(255),
  created_at TIMESTAMPTZ NOT NULL DEFAULT now(),
	updated_at TIMESTAMPTZ NOT NULL DEFAULT now() 
);

-- migrate:down
DROP TABLE IF EXISTS product_images;
