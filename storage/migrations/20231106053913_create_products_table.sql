-- migrate:up
CREATE TABLE products(
  id uuid NOT NULL PRIMARY KEY,
  sku VARCHAR(125) NOT NULL UNIQUE, 
  title VARCHAR(255) NOT NULL,
  category_id UUID CONSTRAINT category_id_fkey REFERENCES categories(id) ON DELETE
    SET NULL,
  description TEXT,
  weight_in_gr INT,
  price BIGINT,
  currency VARCHAR(10),
  created_at TIMESTAMPTZ NOT NULL DEFAULT now(),
	updated_at TIMESTAMPTZ NOT NULL DEFAULT now() 
);

-- migrate:down
DROP TABLE IF NOT EXISTS products;
