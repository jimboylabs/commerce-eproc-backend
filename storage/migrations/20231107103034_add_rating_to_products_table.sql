-- migrate:up
ALTER TABLE products ADD COLUMN rating INT DEFAULT 0;

-- migrate:down
ALTER TABLE products DROP COLUMN IF EXISTS rating;
