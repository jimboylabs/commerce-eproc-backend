package storage

import (
	"context"
	"database/sql"
	"embed"
	"errors"
	"fmt"
	"net/url"
	"time"

	"github.com/amacneil/dbmate/v2/pkg/dbmate"
	_ "github.com/amacneil/dbmate/v2/pkg/driver/postgres"
	"github.com/xo/dburl"
)

//go:embed migrations/*.sql
var migrationFileFs embed.FS

type Storage interface {
	DB() *sql.DB
}

var ErrInvalidDatabaseURL = errors.New("database url is not valid")

type MigrationOptions struct{}

type Options struct {
	DatabaseURL string
}

func (o Options) Validate() error {
	switch {
	case o.DatabaseURL == "":
		return ErrInvalidDatabaseURL
	}

	return nil
}

type Client struct {
	db          *sql.DB
	databaseURL string
	pingTimeout time.Duration
}

func (c *Client) RunMigrations(opt MigrationOptions) error {
	u, err := url.Parse(c.databaseURL)
	if err != nil {
		return err
	}

	db := dbmate.New(u)
	db.MigrationsDir = []string{"./migrations/"}
	db.FS = migrationFileFs

	migrations, err := db.FindMigrations()
	if err != nil {
		panic(err)
	}

	for _, m := range migrations {
		fmt.Println(m.Version, m.FilePath)
	}

	err = db.CreateAndMigrate()
	if err != nil {
		panic(err)
	}

	return nil
}

func New(ctx context.Context, opt Options) (*Client, error) {
	if err := opt.Validate(); err != nil {
		return nil, err
	}

	db, err := dburl.Open(opt.DatabaseURL)
	if err != nil {
		return nil, err
	}

	if err := db.PingContext(ctx); err != nil {
		return nil, err
	}

	return &Client{
		db:          db,
		databaseURL: opt.DatabaseURL,
		pingTimeout: 5 * time.Second,
	}, nil
}

func (c *Client) DB() *sql.DB {
	return c.db
}
