package twirpHandler

import (
	"context"

	"github.com/google/uuid"
	"github.com/spf13/cast"
	"github.com/twitchtv/twirp"
	"gitlab.com/jimboylabs/commerce/common/pagination"
	"gitlab.com/jimboylabs/commerce/dto"
	apiv1 "gitlab.com/jimboylabs/commerce/protos/go/api/v1"
	"gitlab.com/jimboylabs/commerce/service"
)

var _ apiv1.CommerceService = (*twirpHandler)(nil)

type twirpHandler struct {
	productSvc service.ProductService
}

func NewTwirpHandler(productSvc service.ProductService) apiv1.CommerceService {
	return &twirpHandler{productSvc: productSvc}
}

// CreateCategory implements api_v1.CommerceService.
func (h *twirpHandler) CreateCategory(ctx context.Context, req *apiv1.CreateCategoryRequest) (*apiv1.CreateCategoryResponse, error) {
	if req.GetName() == "" {
		return nil, twirp.InvalidArgument.Error("category name is required")
	}

	categoryID, err := h.productSvc.CreateCategory(ctx, req.GetName())
	if err != nil {
		return nil, twirp.Internal.Errorf("DB error: %w", err)
	}

	return &apiv1.CreateCategoryResponse{
		Id: categoryID,
	}, nil
}

func mapCategoriesToAPI(categoriesDto []*dto.CategoryDto) []*apiv1.Category {
	result := make([]*apiv1.Category, 0)
	for _, c := range categoriesDto {
		result = append(result, &apiv1.Category{
			Id:   c.ID,
			Name: c.Name,
		})
	}
	return result
}

// ListCategories implements api_v1.CommerceService.
func (h *twirpHandler) ListCategories(ctx context.Context, req *apiv1.ListCategoriesRequest) (*apiv1.ListCategoriesResponse, error) {
	categories, err := h.productSvc.ListCategories(ctx)
	if err != nil {
		return nil, twirp.Internal.Errorf("DB error: %w", err)
	}

	return &apiv1.ListCategoriesResponse{
		Categories: mapCategoriesToAPI(categories),
	}, nil
}

// CreateProduct implements api_v1.CommerceService.
func (h *twirpHandler) CreateProduct(ctx context.Context, req *apiv1.CreateProductRequest) (*apiv1.CreateProductResponse, error) {
	if req.GetSku() == "" {
		return nil, twirp.InvalidArgument.Error("sku is required")
	}

	if req.GetTitle() == "" {
		return nil, twirp.InvalidArgument.Error("title is required")
	}

	if req.GetCategoryId() == "" {
		return nil, twirp.InvalidArgument.Error("category name is required")
	}

	productID, err := h.productSvc.CreateProduct(ctx, dto.CreateProductDto{
		SKU:         req.GetSku(),
		Title:       req.GetTitle(),
		Description: req.GetDescription(),
		CategoryID:  req.GetCategoryId(),
		Price:       cast.ToInt64(req.GetPrice()),
		Currency:    req.GetCurrency(),
	})
	if err != nil {
		return nil, twirp.Internal.Errorf("DB error: %w", err)
	}

	return &apiv1.CreateProductResponse{Id: productID}, nil
}

// GetProduct implements api_v1.CommerceService.
func (h *twirpHandler) GetProduct(ctx context.Context, req *apiv1.GetProductRequest) (*apiv1.GetProductResponse, error) {
	productID, err := uuid.Parse(req.GetId())
	if err != nil {
		return nil, twirp.InvalidArgument.Error("invalid product id")
	}

	product, err := h.productSvc.FindProductByID(ctx, &productID)
	if err != nil {
		return nil, twirp.Internal.Errorf("DB error: %w", err)
	}

	return &apiv1.GetProductResponse{Product: &apiv1.Product{
		Id:          product.ID,
		Sku:         product.SKU,
		Title:       product.Title,
		Description: product.Description,
		Price:       product.Price,
		Currency:    product.Currency,
		Category: &apiv1.Category{
			Id:   product.Category.ID,
			Name: product.Category.Name,
		},
		Rating: uint32(product.Rating),
	}}, nil
}

// UpdateProduct implements api_v1.CommerceService.
func (h *twirpHandler) UpdateProduct(ctx context.Context, req *apiv1.UpdateProductRequest) (*apiv1.UpdateProductResponse, error) {
	if req.GetId() == "" {
		return nil, twirp.InvalidArgument.Error("product id is required")
	}

	product, err := h.productSvc.UpdateProduct(ctx, dto.UpdateProductDto{
		ID:          req.GetId(),
		SKU:         req.GetSku(),
		Title:       req.GetTitle(),
		Description: req.GetDescription(),
		CategoryID:  req.GetCategoryId(),
	})
	if err != nil {
		return nil, twirp.Internal.Errorf("DB error: %w", err)
	}

	return &apiv1.UpdateProductResponse{
		Product: &apiv1.Product{
			Id:          product.ID,
			Sku:         product.SKU,
			Title:       product.Title,
			Description: product.Description,
			Price:       product.Price,
			Currency:    product.Currency,
			Category: &apiv1.Category{
				Id:   product.Category.ID,
				Name: product.Category.Name,
			},
			Rating: uint32(product.Rating),
		},
	}, nil
}

func mapSortBy(sortBy apiv1.ListProductsRequest_SortBy) string {
	switch sortBy {
	case apiv1.ListProductsRequest_SORT_BY_RATING:
		return "rating"
	default:
		return "created_at"
	}
}

func mapSortDirection(direction apiv1.ListProductsRequest_SortDirection) string {
	switch direction {
	case apiv1.ListProductsRequest_SORT_DIRECTION_ASC:
		return "ASC"
	default:
		return "DESC"
	}
}

func mapCategoryToAPI(categoryDto dto.CategoryDto) *apiv1.Category {
	return &apiv1.Category{
		Id:   categoryDto.ID,
		Name: categoryDto.Name,
	}
}

func mapProductImagesToAPI(productID string, imagesDto []dto.ProductImageDto) []*apiv1.ProductImage {
	result := make([]*apiv1.ProductImage, 0)
	for _, m := range imagesDto {
		result = append(result, &apiv1.ProductImage{
			Id:        m.ID,
			ProductId: productID,
			ImageUrl:  m.ImageURL,
			ShortDesc: m.ShortDesc,
		})
	}
	return result
}

// ListProducts implements api_v1.CommerceService.
func (h *twirpHandler) ListProducts(ctx context.Context, req *apiv1.ListProductsRequest) (*apiv1.ListProductsResponse, error) {
	page := uint64(req.GetPage())
	pageSize := uint64(req.GetPageSize())
	pageParam := pagination.ParsePageParam(page, pageSize, pagination.DEFAULT_PAGE_SIZE)
	sortBy := mapSortBy(req.GetSortBy())
	sortDirection := mapSortDirection(req.GetSortDirection())

	result, err := h.productSvc.ListProducts(ctx, dto.ListProductsFilterDto{
		SKU:        req.GetSku(),
		Title:      req.GetTitle(),
		CategoryID: req.GetCategoryId(),
	}, pageParam, sortBy, sortDirection)
	if err != nil {
		return nil, twirp.Internal.Errorf("DB error: %w", err)
	}

	products := make([]*apiv1.Product, 0)

	for _, p := range result.Products {
		products = append(products, &apiv1.Product{
			Id:          p.ID,
			Sku:         p.SKU,
			Title:       p.Title,
			Description: p.Description,
			Price:       p.Price,
			Currency:    p.Currency,
			Category:    mapCategoryToAPI(p.Category),
			Rating:      uint32(p.Rating),
			Images:      mapProductImagesToAPI(p.ID, p.Images),
		})
	}

	return &apiv1.ListProductsResponse{
		Products: products,
		Page:     uint32(result.Page),
		PageSize: uint32(result.PageSize),
		HasPrev:  result.HasPrev,
		HasNext:  result.HasNext,
	}, nil
}

// CreateReview implements api_v1.CommerceService.
func (h *twirpHandler) CreateReview(ctx context.Context, req *apiv1.CreateReviewRequest) (*apiv1.CreateReviewResponse, error) {
	if req.GetMessage() == "" {
		return nil, twirp.InvalidArgument.Error("message is required")
	}

	if req.GetRating() < 10 || req.GetRating() > 50 {
		return nil, twirp.InvalidArgument.Error("rating should be between 10-50")
	}

	if req.GetProductId() == "" {
		return nil, twirp.InvalidArgument.Error("product is is required")
	}

	reviewID, err := h.productSvc.CreateReview(ctx, dto.CreateReviewDto{
		ProductID: req.GetProductId(),
		Message:   req.GetMessage(),
		Rating:    uint(req.GetRating()),
	})
	if err != nil {
		return nil, twirp.Internal.Errorf("DB error: %w", err)
	}

	return &apiv1.CreateReviewResponse{
		Id: reviewID,
	}, nil
}

// CreateProductImage implements api_v1.CommerceService.
func (h *twirpHandler) CreateProductImage(ctx context.Context, req *apiv1.CreateProductImageRequest) (*apiv1.CreateProductImageResponse, error) {
	if req.GetProductId() == "" {
		return nil, twirp.InvalidArgument.Error("product id is required")
	}

	if req.GetImageUrl() == "" {
		return nil, twirp.InvalidArgument.Error("image url is required")
	}

	imageID, err := h.productSvc.CreateProductImage(ctx, dto.CreateProductImageDto{
		ProductID: req.GetProductId(),
		ImageURL:  req.GetImageUrl(),
		ShortDesc: req.GetShortDesc(),
	})
	if err != nil {
		return nil, twirp.Internal.Errorf("DB error: %w", err)
	}

	return &apiv1.CreateProductImageResponse{
		Id: imageID,
	}, nil
}
