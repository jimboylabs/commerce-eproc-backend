package repository

import (
	sq "github.com/Masterminds/squirrel"
	"gitlab.com/jimboylabs/commerce/storage"
)

const (
	CategoriesTable    = "categories"
	ProductsTable      = "products"
	ReviewsTable       = "reviews"
	ProductImagesTable = "product_images"
)

type CommonRepository struct {
	storage.Storage
}

func (c *CommonRepository) Builder() sq.StatementBuilderType {
	return sq.StatementBuilder.PlaceholderFormat(sq.Dollar).RunWith(c.DB())
}
