package product

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"strings"
	"time"

	sq "github.com/Masterminds/squirrel"
	"github.com/google/uuid"
	"gitlab.com/jimboylabs/commerce/common/pagination"
	"gitlab.com/jimboylabs/commerce/repository"
	"gitlab.com/jimboylabs/commerce/storage"
)

const DefaultCurrency = "IDR"

var _ Repository = (*repo)(nil)

type Product struct {
	ID          string
	SKU         string
	Title       string
	Description string
	Currency    string
	WeightInGr  int
	Price       sql.NullInt64
	CategoryID  string
	Rating      uint

	CreatedAt *time.Time
	UpdatedAt *time.Time
}

type ListProductsFilter struct {
	SKU        string
	Title      string
	CategoryID string

	SortBy        string
	SortDirection string
}

func CalcNewRating(product Product, newRating uint) uint {
	if product.Rating == 0 {
		return newRating
	}

	return (product.Rating + newRating) / 2
}

type Repository interface {
	Create(ctx context.Context, product *Product) error
	FindByID(ctx context.Context, productID string) (*Product, error)
	Update(ctx context.Context, product *Product) error
	List(ctx context.Context, filter ListProductsFilter, pageParam pagination.PageParam, sortBy, sortDirection string) ([]*Product, error)
}

type repo struct {
	repository.CommonRepository
}

func NewProductRepository(conn storage.Storage) Repository {
	return &repo{repository.CommonRepository{Storage: conn}}
}

// GetOrCreate implements Repository.
func (r *repo) Create(ctx context.Context, product *Product) error {
	product.ID = uuid.NewString()

	if product.Currency == "" {
		product.Currency = DefaultCurrency
	}

	err := r.Builder().Insert(repository.ProductsTable).
		Columns(
			"id",
			"sku",
			"title",
			"description",
			"category_id",
			"weight_in_gr",
			"price",
			"currency",
		).Values(
		product.ID,
		product.SKU,
		product.Title,
		product.Description,
		product.CategoryID,
		product.WeightInGr,
		product.Price,
		product.Currency,
	).Suffix("ON CONFLICT(sku) DO UPDATE SET sku = excluded.sku RETURNING id").
		QueryRowContext(ctx).
		Scan(&product.ID)

	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return fmt.Errorf("product.get_or_create: %s: %w", product.SKU, err)
	}

	return nil
}

// FindByID implements Repository.
func (r *repo) FindByID(ctx context.Context, productID string) (*Product, error) {
	result := &Product{}
	err := r.Builder().
		Select("id", "sku", "title", "description", "category_id", "weight_in_gr", "price", "currency", "rating").
		From(repository.ProductsTable).
		Where(sq.Eq{"id": productID}).
		QueryRowContext(ctx).
		Scan(
			&result.ID,
			&result.SKU,
			&result.Title,
			&result.Description,
			&result.CategoryID,
			&result.WeightInGr,
			&result.Price,
			&result.Currency,
			&result.Rating,
		)
	return result, err
}

// Update implements Repository.
func (r *repo) Update(ctx context.Context, product *Product) error {
	_, err := r.Builder().
		Update(repository.ProductsTable).
		Set("sku", product.SKU).
		Set("title", product.Title).
		Set("description", product.Description).
		Set("category_id", product.CategoryID).
		Set("rating", product.Rating).
		Where(sq.Eq{"id": product.ID}).
		ExecContext(ctx)
	return err
}

// List implements Repository.
func (r *repo) List(ctx context.Context, filter ListProductsFilter, pageParam pagination.PageParam, sortBy, sortDirection string) ([]*Product, error) {
	builder := r.Builder().
		Select("id", "sku", "title", "description", "category_id", "weight_in_gr", "price", "currency", "rating").
		From(repository.ProductsTable)

	if filter.SKU != "" {
		builder = builder.Where(sq.Like{"sku": "%" + filter.SKU + "%"})
	}

	if filter.Title != "" {
		builder = builder.Where(sq.Like{"title": "%" + filter.Title + "%"})
	}

	if filter.CategoryID != "" {
		builder = builder.Where(sq.Eq{"category_id": filter.CategoryID})
	}

	orderBy := "created_at DESC"

	if sortBy != "" && sortDirection != "" {
		orderBy = fmt.Sprintf("%s %s", sortBy, strings.ToUpper(sortDirection))
	}

	rows, err := builder.
		Limit(pageParam.Limit).
		Offset(pageParam.Offset).
		OrderBy(orderBy).
		QueryContext(ctx)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := []*Product{}
	for rows.Next() {
		record := Product{}
		err := rows.Scan(
			&record.ID,
			&record.SKU,
			&record.Title,
			&record.Description,
			&record.CategoryID,
			&record.WeightInGr,
			&record.Price,
			&record.Currency,
			&record.Rating,
		)
		if err != nil {
			continue
		}
		result = append(result, &record)
	}

	return result, nil
}
