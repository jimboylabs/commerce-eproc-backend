package product

import "testing"

func TestCalcNewRating(t *testing.T) {
	tests := []struct {
		Name         string
		Rating       uint
		NewRating    uint
		WantedRating uint
	}{
		{
			Name:         "should return 50",
			Rating:       50,
			NewRating:    50,
			WantedRating: 50,
		},
		{
			Name:         "should return return 30",
			Rating:       10,
			NewRating:    50,
			WantedRating: 30,
		},
		{
			Name:         "should return return 40",
			Rating:       0,
			NewRating:    40,
			WantedRating: 40,
		},
	}

	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			got := CalcNewRating(Product{
				Rating: tt.Rating,
			}, tt.NewRating)

			if got != tt.WantedRating {
				t.Errorf("got: %d want: %d", got, tt.WantedRating)
			}
		})
	}
}
