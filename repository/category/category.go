package category

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	sq "github.com/Masterminds/squirrel"
	"github.com/google/uuid"
	"gitlab.com/jimboylabs/commerce/common/log"
	"gitlab.com/jimboylabs/commerce/repository"
	"gitlab.com/jimboylabs/commerce/storage"
)

var _ Repository = (*repo)(nil)

type Category struct {
	ID        string
	Name      string
	CreatedAt *time.Time
	UpdatedAt *time.Time
}

type Repository interface {
	Create(ctx context.Context, name string) (*Category, error)
	FindByID(ctx context.Context, categoryID string) (*Category, error)
	FindByIDs(ctx context.Context, categoryIDs []string) (map[string]*Category, error)
}

type repo struct {
	repository.CommonRepository
}

func NewCategoryRepository(conn storage.Storage) Repository {
	return &repo{repository.CommonRepository{Storage: conn}}
}

// GetOrCreate implements Repository.
func (r *repo) Create(ctx context.Context, name string) (*Category, error) {
	category := &Category{
		ID:   uuid.NewString(),
		Name: name,
	}

	err := r.Builder().Insert(repository.CategoriesTable).
		Columns("id", "name").
		Values(category.ID, category.Name).
		Suffix("ON CONFLICT(name) DO UPDATE SET name = excluded.name RETURNING id").
		QueryRowContext(ctx).
		Scan(&category.ID)

	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return nil, fmt.Errorf("category.get_or_create: %s: %w", name, err)
	}

	return category, nil
}

// FindByID implements Repository.
func (r *repo) FindByID(ctx context.Context, categoryID string) (*Category, error) {
	result := &Category{}
	err := r.Builder().
		Select("id", "name").
		From(repository.CategoriesTable).
		Where(sq.Eq{"id": categoryID}).
		QueryRowContext(ctx).
		Scan(&result.ID, &result.Name)

	return result, err
}

// FindByIDs implements Repository.
func (r *repo) FindByIDs(ctx context.Context, categoryIDs []string) (map[string]*Category, error) {
	builder := r.Builder().
		Select("id", "name").
		From(repository.CategoriesTable)

	if len(categoryIDs) > 0 {
		builder = builder.Where(sq.Eq{"id": categoryIDs})
	}

	rows, err := builder.
		QueryContext(ctx)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	logger := log.FromContext(ctx)
	result := make(map[string]*Category, 0)

	for rows.Next() {
		record := Category{}
		err := rows.Scan(&record.ID, &record.Name)
		if err != nil {
			logger.Debug("error rows.Scan categoryIDs: ", categoryIDs)
			continue
		}

		_, ok := result[record.ID]
		if ok {
			continue
		}

		result[record.ID] = &record
	}

	return result, nil
}
