package review

import (
	"context"

	"github.com/google/uuid"
	"gitlab.com/jimboylabs/commerce/repository"
	"gitlab.com/jimboylabs/commerce/storage"
)

var _ Repository = (*repo)(nil)

type Review struct {
	ID        string
	ProductID string
	Message   string
	Rating    uint
}

type Repository interface {
	Create(ctx context.Context, review *Review) error
}

type repo struct {
	repository.CommonRepository
}

func NewReviewRepository(conn storage.Storage) Repository {
	return &repo{repository.CommonRepository{Storage: conn}}
}

// Create implements Repository.
func (r *repo) Create(ctx context.Context, review *Review) error {
	err := r.Builder().Insert(repository.ReviewsTable).
		Columns("id", "product_id", "message", "rating").
		Values(uuid.NewString(), review.ProductID, review.Message, review.Rating).
		Suffix("RETURNING id").
		QueryRowContext(ctx).
		Scan(&review.ID)
	return err
}
