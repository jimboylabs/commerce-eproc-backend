package productImage

import (
	"context"

	sq "github.com/Masterminds/squirrel"
	"github.com/google/uuid"
	"gitlab.com/jimboylabs/commerce/common/log"
	"gitlab.com/jimboylabs/commerce/repository"
	"gitlab.com/jimboylabs/commerce/storage"
)

var _ Repository = (*repo)(nil)

type ProductImage struct {
	ID        string
	ProductID string
	ImageURL  string
	ShortDesc string
}

type Repository interface {
	Create(ctx context.Context, image *ProductImage) error
	FindByProductIDs(ctx context.Context, productIDs []string) (images map[string][]*ProductImage, err error)
}

type repo struct {
	repository.CommonRepository
}

func NewProductImageRepository(conn storage.Storage) Repository {
	return &repo{repository.CommonRepository{Storage: conn}}
}

// Create implements Repository.
func (r *repo) Create(ctx context.Context, image *ProductImage) error {
	image.ID = uuid.NewString()
	err := r.Builder().Insert(repository.ProductImagesTable).
		Columns("id", "product_id", "image_url", "short_desc").
		Values(image.ID, image.ProductID, image.ImageURL, image.ShortDesc).
		Suffix("RETURNING id").
		QueryRowContext(ctx).
		Scan(&image.ID)
	return err
}

// FindByProductIDs implements Repository.
func (r *repo) FindByProductIDs(ctx context.Context, productIDs []string) (images map[string][]*ProductImage, err error) {
	rows, err := r.Builder().
		Select("id", "product_id", "image_url", "short_desc").
		From(repository.ProductImagesTable).
		Where(sq.Eq{"product_id": productIDs}).
		OrderBy("created_at DESC").
		QueryContext(ctx)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	logger := log.FromContext(ctx)

	images = make(map[string][]*ProductImage, 0)

	for rows.Next() {
		record := ProductImage{}
		err := rows.Scan(&record.ID, &record.ProductID, &record.ImageURL, &record.ShortDesc)
		if err != nil {
			logger.Debug("error rows.Scan productIDs: ", productIDs)
			continue
		}

		v, ok := images[record.ProductID]
		if !ok {
			images[record.ProductID] = []*ProductImage{&record}
			continue
		}

		images[record.ProductID] = append(v, &record)
	}

	return
}
