package http

import (
	"errors"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/jimboylabs/commerce/common/log"
)

func NewEcho() *echo.Echo {
	e := echo.New()

	useMiddlewares(e)
	e.HTTPErrorHandler = HandleError

	return e
}

func HandleError(err error, c echo.Context) {
	log.FromContext(c.Request().Context()).WithError(err).Error("HTTP error")

	httpCode := http.StatusInternalServerError
	msg := any("Internal server error")

	httpErr := &echo.HTTPError{}
	if errors.As(err, &httpErr) {
		httpCode = httpErr.Code
		msg = httpErr.Message
	}

	jsonErr := c.JSON(httpCode, map[string]any{
		"error": msg,
	})
	if jsonErr != nil {
		panic(err)
	}
}
