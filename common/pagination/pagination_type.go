package pagination

type PageParam struct {
	Page     uint64
	PageSize uint64
	Offset   uint64
	Limit    uint64
}

type PaginateData struct {
	Page     uint64
	PageSize uint64
	HasNext  bool
	HasPrev  bool
	Items    any
}
