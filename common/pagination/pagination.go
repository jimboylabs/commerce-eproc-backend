package pagination

import "reflect"

const DEFAULT_PAGE_SIZE uint64 = 20

func ParsePageParam(page, pageSize, defaultPageSize uint64) PageParam {
	newPage := page
	if newPage <= 0 {
		newPage = 1
	}

	newPageSize := pageSize
	if newPageSize <= 0 {
		newPageSize = defaultPageSize
	}

	return PageParam{
		Page:     newPage,
		PageSize: newPageSize,
		Offset:   (newPage - 1) * newPageSize,
		Limit:    newPageSize + 1,
	}
}

func Paginate(items any, pageParam PageParam) PaginateData {
	if reflect.Slice != reflect.TypeOf(items).Kind() {
		panic("paginated items must be a slice")
	}

	hasPrev := true
	if pageParam.Page <= 1 {
		hasPrev = false
	}

	s := reflect.ValueOf(items)
	resultItems := s.Interface()
	pageSize := s.Len()
	hasNext := true
	if uint64(s.Len()) <= pageParam.PageSize {
		hasNext = false
	}
	if hasNext {
		pageSize = s.Len() - 1
		resultItems = s.Slice(0, s.Len()-1).Interface()
	}
	return PaginateData{
		Page:     pageParam.Page,
		PageSize: uint64(pageSize),
		HasNext:  hasNext,
		HasPrev:  hasPrev,
		Items:    resultItems,
	}
}
