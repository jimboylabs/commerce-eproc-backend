package slice

func AppendUniqueString(arr []string, value string) []string {
	for _, v := range arr {
		if v == value {
			return arr
		}
	}

	return append(arr, value)
}
