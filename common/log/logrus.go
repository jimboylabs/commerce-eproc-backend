package log

import "github.com/sirupsen/logrus"

func Init(level logrus.Level) {
	logrus.SetLevel(level)

	logrus.SetFormatter(&logrus.TextFormatter{
		ForceColors:      true,
		ForceQuote:       false,
		DisableQuote:     true,
		FullTimestamp:    true,
		TimestampFormat:  "15:05:05.0000",
		QuoteEmptyFields: true,
	})
}
