# CommerceService

`api.v1.CommerceService`

### Methods

* [CreateCategory](#post-createcategory)
* [ListCategories](#post-listcategories)
* [CreateProduct](#post-createproduct)
* [GetProduct](#post-getproduct)
* [UpdateProduct](#post-updateproduct)
* [ListProducts](#post-listproducts)
* [CreateReview](#post-createreview)
* [CreateProductImage](#post-createproductimage)

### Models

* [api.v1.Category](#apiv1category)
* [api.v1.ListProductsRequest.SortBy](#apiv1listproductsrequestsortby)
* [api.v1.ListProductsRequest.SortDirection](#apiv1listproductsrequestsortdirection)
* [api.v1.Product](#apiv1product)
* [api.v1.ProductImage](#apiv1productimage)

---

## Methods

Base URL: `https://api.example.com/twirp/api.v1.CommerceService`

### POST `/CreateCategory`

#### Request

`api.v1.CreateCategoryRequest`

`POST /api.v1.CommerceService/CreateCategory`

```json
{
  "name": "foo"
}
```

| Field  |  Type  | Description |
|:-------|:------:|:------------|
| `name` | string |             |

#### Response

`api.v1.CreateCategoryResponse`

`HTTP 200 OK`

```json
{
  "id": "foo"
}
```

| Field |  Type  | Description |
|:------|:------:|:------------|
| `id`  | string |             |

### POST `/ListCategories`

#### Request

`api.v1.ListCategoriesRequest`

`POST /api.v1.CommerceService/ListCategories`

```json
{}
```

| Field | Type | Description |
|:------|:----:|:------------|

#### Response

`api.v1.ListCategoriesResponse`

`HTTP 200 OK`

```json
{
  "categories": [
    {
      "id": "foo",
      "name": "foo"
    },
    {
      "id": "bar",
      "name": "bar"
    },
    {
      "id": "baz",
      "name": "baz"
    }
  ]
}
```

| Field        |                    Type                    | Description |
|:-------------|:------------------------------------------:|:------------|
| `categories` | array of [api.v1.Category](#apiv1category) |             |

### POST `/CreateProduct`

#### Request

`api.v1.CreateProductRequest`

`POST /api.v1.CommerceService/CreateProduct`

```json
{
  "sku": "foo",
  "title": "foo",
  "description": "foo",
  "price": "foo",
  "currency": "foo",
  "categoryId": "foo"
}
```

| Field         |  Type  | Description |
|:--------------|:------:|:------------|
| `sku`         | string |             |
| `title`       | string |             |
| `description` | string |             |
| `price`       | string |             |
| `currency`    | string |             |
| `categoryId`  | string |             |

#### Response

`api.v1.CreateProductResponse`

`HTTP 200 OK`

```json
{
  "id": "foo"
}
```

| Field |  Type  | Description |
|:------|:------:|:------------|
| `id`  | string |             |

### POST `/GetProduct`

#### Request

`api.v1.GetProductRequest`

`POST /api.v1.CommerceService/GetProduct`

```json
{
  "id": "foo"
}
```

| Field |  Type  | Description |
|:------|:------:|:------------|
| `id`  | string |             |

#### Response

`api.v1.GetProductResponse`

`HTTP 200 OK`

```json
{
  "product": {
    "id": "foo",
    "sku": "foo",
    "title": "foo",
    "description": "foo",
    "price": "foo",
    "currency": "foo",
    "category": {
      "id": "foo",
      "name": "foo"
    },
    "rating": 1073741824,
    "images": [
      {
        "id": "foo",
        "productId": "foo",
        "imageUrl": "foo",
        "shortDesc": "foo"
      },
      {
        "id": "bar",
        "productId": "bar",
        "imageUrl": "bar",
        "shortDesc": "bar"
      },
      {
        "id": "baz",
        "productId": "baz",
        "imageUrl": "baz",
        "shortDesc": "baz"
      }
    ]
  }
}
```

| Field     |              Type               | Description |
|:----------|:-------------------------------:|:------------|
| `product` | [api.v1.Product](#apiv1product) |             |

### POST `/UpdateProduct`

#### Request

`api.v1.UpdateProductRequest`

`POST /api.v1.CommerceService/UpdateProduct`

```json
{
  "id": "foo",
  "sku": "foo",
  "title": "foo",
  "description": "foo",
  "price": "foo",
  "currency": "foo",
  "categoryId": "foo"
}
```

| Field         |  Type  | Description |
|:--------------|:------:|:------------|
| `id`          | string |             |
| `sku`         | string |             |
| `title`       | string |             |
| `description` | string |             |
| `price`       | string |             |
| `currency`    | string |             |
| `categoryId`  | string |             |

#### Response

`api.v1.UpdateProductResponse`

`HTTP 200 OK`

```json
{
  "product": {
    "id": "foo",
    "sku": "foo",
    "title": "foo",
    "description": "foo",
    "price": "foo",
    "currency": "foo",
    "category": {
      "id": "foo",
      "name": "foo"
    },
    "rating": 1073741824,
    "images": [
      {
        "id": "foo",
        "productId": "foo",
        "imageUrl": "foo",
        "shortDesc": "foo"
      },
      {
        "id": "bar",
        "productId": "bar",
        "imageUrl": "bar",
        "shortDesc": "bar"
      },
      {
        "id": "baz",
        "productId": "baz",
        "imageUrl": "baz",
        "shortDesc": "baz"
      }
    ]
  }
}
```

| Field     |              Type               | Description |
|:----------|:-------------------------------:|:------------|
| `product` | [api.v1.Product](#apiv1product) |             |

### POST `/ListProducts`

#### Request

`api.v1.ListProductsRequest`

`POST /api.v1.CommerceService/ListProducts`

```json
{
  "page": 1073741824,
  "pageSize": 1073741824,
  "sortBy": "SORT_BY_CREATED_AT",
  "sortDirection": "SORT_DIRECTION_ASC",
  "sku": "foo",
  "title": "foo",
  "categoryId": "foo"
}
```

| Field           |                           Type                            | Description                               |
|:----------------|:---------------------------------------------------------:|:------------------------------------------|
| `page`          |                           int32                           |                                           |
| `pageSize`      |                           int32                           | The maximum number of products to return. |
| `sortBy`        |        [api.v1.ListProductsRequest.SortBy](SortBy)        |                                           |
| `sortDirection` | [api.v1.ListProductsRequest.SortDirection](SortDirection) |                                           |
| `sku`           |                          string                           | Filter                                    |
| `title`         |                          string                           |                                           |
| `categoryId`    |                          string                           |                                           |

#### Response

`api.v1.ListProductsResponse`

`HTTP 200 OK`

```json
{
  "products": [
    {
      "id": "foo",
      "sku": "foo",
      "title": "foo",
      "description": "foo",
      "price": "foo",
      "currency": "foo",
      "category": {
        "id": "foo",
        "name": "foo"
      },
      "rating": 1073741824,
      "images": [
        {
          "id": "foo",
          "productId": "foo",
          "imageUrl": "foo",
          "shortDesc": "foo"
        },
        {
          "id": "bar",
          "productId": "bar",
          "imageUrl": "bar",
          "shortDesc": "bar"
        },
        {
          "id": "baz",
          "productId": "baz",
          "imageUrl": "baz",
          "shortDesc": "baz"
        }
      ]
    },
    {
      "id": "bar",
      "sku": "bar",
      "title": "bar",
      "description": "bar",
      "price": "bar",
      "currency": "bar",
      "category": {
        "id": "foo",
        "name": "foo"
      },
      "rating": 1073741824,
      "images": [
        {
          "id": "foo",
          "productId": "foo",
          "imageUrl": "foo",
          "shortDesc": "foo"
        },
        {
          "id": "bar",
          "productId": "bar",
          "imageUrl": "bar",
          "shortDesc": "bar"
        },
        {
          "id": "baz",
          "productId": "baz",
          "imageUrl": "baz",
          "shortDesc": "baz"
        }
      ]
    },
    {
      "id": "baz",
      "sku": "baz",
      "title": "baz",
      "description": "baz",
      "price": "baz",
      "currency": "baz",
      "category": {
        "id": "foo",
        "name": "foo"
      },
      "rating": 1073741824,
      "images": [
        {
          "id": "foo",
          "productId": "foo",
          "imageUrl": "foo",
          "shortDesc": "foo"
        },
        {
          "id": "bar",
          "productId": "bar",
          "imageUrl": "bar",
          "shortDesc": "bar"
        },
        {
          "id": "baz",
          "productId": "baz",
          "imageUrl": "baz",
          "shortDesc": "baz"
        }
      ]
    }
  ],
  "page": 1073741824,
  "pageSize": 1073741824,
  "hasPrev": true,
  "hasNext": true
}
```

| Field      |                   Type                   | Description |
|:-----------|:----------------------------------------:|:------------|
| `products` | array of [api.v1.Product](#apiv1product) |             |
| `page`     |                  uint32                  |             |
| `pageSize` |                  uint32                  |             |
| `hasPrev`  |                   bool                   |             |
| `hasNext`  |                   bool                   |             |

### POST `/CreateReview`

#### Request

`api.v1.CreateReviewRequest`

`POST /api.v1.CommerceService/CreateReview`

```json
{
  "productId": "foo",
  "message": "foo",
  "rating": 1073741824
}
```

| Field       |  Type  | Description |
|:------------|:------:|:------------|
| `productId` | string |             |
| `message`   | string |             |
| `rating`    | uint32 |             |

#### Response

`api.v1.CreateReviewResponse`

`HTTP 200 OK`

```json
{
  "id": "foo"
}
```

| Field |  Type  | Description |
|:------|:------:|:------------|
| `id`  | string |             |

### POST `/CreateProductImage`

#### Request

`api.v1.CreateProductImageRequest`

`POST /api.v1.CommerceService/CreateProductImage`

```json
{
  "productId": "foo",
  "imageUrl": "foo",
  "shortDesc": "foo"
}
```

| Field       |  Type  | Description |
|:------------|:------:|:------------|
| `productId` | string |             |
| `imageUrl`  | string |             |
| `shortDesc` | string |             |

#### Response

`api.v1.CreateProductImageResponse`

`HTTP 200 OK`

```json
{
  "id": "foo"
}
```

| Field |  Type  | Description |
|:------|:------:|:------------|
| `id`  | string |             |

## Models

### api.v1.Category

| Field  |  Type  | Description |
|:-------|:------:|:------------|
| `id`   | string |             |
| `name` | string |             |

### api.v1.ListProductsRequest.SortBy

| Value                 | Description |
|:----------------------|:------------|
| `SORT_BY_UNSPECIFIED` |             |
| `SORT_BY_CREATED_AT`  |             |
| `SORT_BY_RATING`      |             |

### api.v1.ListProductsRequest.SortDirection

| Value                        | Description |
|:-----------------------------|:------------|
| `SORT_DIRECTION_UNSPECIFIED` |             |
| `SORT_DIRECTION_ASC`         |             |
| `SORT_DIRECTION_DESC`        |             |

### api.v1.Product

| Field         |                        Type                        | Description |
|:--------------|:--------------------------------------------------:|:------------|
| `id`          |                       string                       |             |
| `sku`         |                       string                       |             |
| `title`       |                       string                       |             |
| `description` |                       string                       |             |
| `price`       |                       string                       |             |
| `currency`    |                       string                       |             |
| `category`    |         [api.v1.Category](#apiv1category)          |             |
| `rating`      |                       uint32                       |             |
| `images`      | array of [api.v1.ProductImage](#apiv1productimage) |             |

### api.v1.ProductImage

| Field       |  Type  | Description |
|:------------|:------:|:------------|
| `id`        | string |             |
| `productId` | string |             |
| `imageUrl`  | string |             |
| `shortDesc` | string |             |

## Twirp Errors

[Official documentation](https://twitchtv.github.io/twirp/docs/spec_v7.html#error-codes)

| Twirp Error Code      | HTTP Status | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
|:----------------------|:-----------:|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `invalid_argument`    |    `400`    | The client specified an invalid argument. This indicates arguments that are invalid regardless of the state of the system (i.e. a malformed file name, required argument, number out of range, etc.).                                                                                                                                                                                                                                                                                                                                                              |
| `malformed`           |    `400`    | The client sent a message which could not be decoded. This may mean that the message was encoded improperly or that the client and server have incompatible message definitions.                                                                                                                                                                                                                                                                                                                                                                                   |
| `out_of_range`        |    `400`    | The operation was attempted past the valid range. For example, seeking or reading past end of a paginated collection. Unlike "invalid_argument", this error indicates a problem that may be fixed if the system state changes (i.e. adding more items to the collection). There is a fair bit of overlap between "failed_precondition" and "out_of_range". We recommend using "out_of_range" (the more specific error) when it applies so that callers who are iterating through a space can easily look for an "out_of_range" error to detect when they are done. |
| `unauthenticated`     |    `401`    | The request does not have valid authentication credentials for the operation.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| `permission_denied`   |    `403`    | The caller does not have permission to execute the specified operation. It must not be used if the caller cannot be identified (use "unauthenticated" instead).                                                                                                                                                                                                                                                                                                                                                                                                    |
| `bad_route`           |    `404`    | The requested URL path wasn't routable to a Twirp service and method. This is returned by generated server code and should not be returned by application code (use "not_found" or "unimplemented" instead).                                                                                                                                                                                                                                                                                                                                                       |
| `not_found`           |    `404`    | Some requested entity was not found.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| `canceled`            |    `408`    | The operation was cancelled.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `deadline_exceeded`   |    `408`    | Operation expired before completion. For operations that change the state of the system, this error may be returned even if the operation has completed successfully (timeout).                                                                                                                                                                                                                                                                                                                                                                                    |
| `already_exists`      |    `409`    | An attempt to create an entity failed because one already exists.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| `aborted`             |    `409`    | The operation was aborted, typically due to a concurrency issue like sequencer check failures, transaction aborts, etc.                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| `failed_precondition` |    `412`    | The operation was rejected because the system is not in a state required for the operation's execution. For example, doing an rmdir operation on a directory that is non-empty, or on a non-directory object, or when having conflicting read-modify-write on the same resource.                                                                                                                                                                                                                                                                                   |
| `resource_exhausted`  |    `429`    | Some resource has been exhausted or rate-limited, perhaps a per-user quota, or perhaps the entire file system is out of space.                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| `unknown`             |    `500`    | An unknown error occurred. For example, this can be used when handling errors raised by APIs that do not return any error information.                                                                                                                                                                                                                                                                                                                                                                                                                             |
| `internal`            |    `500`    | When some invariants expected by the underlying system have been broken. In other words, something bad happened in the library or backend service. Twirp specific issues like wire and serialization problems are also reported as "internal" errors.                                                                                                                                                                                                                                                                                                              |
| `unavailable`         |    `500`    | The service is currently unavailable. This is most likely a transient condition and may be corrected by retrying with a backoff.                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| `unimplemented`       |    `501`    | The operation is not implemented or not supported/enabled in this service.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| `dataloss`            |    `503`    | The operation resulted in unrecoverable data loss or corruption.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
