package main

import (
	"context"

	"github.com/sethvargo/go-envconfig"
)

type Config struct {
	Environment string `env:"ENVIRONMENT,default=dev"`

	Storage struct {
		RunMigrations bool `env:"DATABASE_RUN_MIGRATIONS,default=true"`
		Postgres      struct {
			DatabaseURL string `env:"DATABASE_URL,required"`
		}
	}
}

func LoadConfig(ctx context.Context) (Config, error) {
	config := Config{}
	if err := envconfig.Process(ctx, &config); err != nil {
		return config, err
	}

	return config, nil
}
