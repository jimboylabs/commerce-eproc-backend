package main

import (
	"context"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	commonHTTP "gitlab.com/jimboylabs/commerce/common/http"
	"gitlab.com/jimboylabs/commerce/common/log"
	"gitlab.com/jimboylabs/commerce/handler/twirpHandler"
	apiv1 "gitlab.com/jimboylabs/commerce/protos/go/api/v1"
	"gitlab.com/jimboylabs/commerce/repository/category"
	"gitlab.com/jimboylabs/commerce/repository/product"
	"gitlab.com/jimboylabs/commerce/repository/productImage"
	"gitlab.com/jimboylabs/commerce/repository/review"
	"gitlab.com/jimboylabs/commerce/service"
	"gitlab.com/jimboylabs/commerce/storage"
	"golang.org/x/sync/errgroup"
)

func main() {
	log.Init(logrus.DebugLevel)

	ctx := context.Background()

	config, err := LoadConfig(ctx)
	if err != nil {
		panic(err)
	}

	db, err := storage.New(ctx, storage.Options{
		DatabaseURL: config.Storage.Postgres.DatabaseURL,
	})
	if err != nil {
		panic(err)
	}

	if config.Storage.RunMigrations {
		err := db.RunMigrations(storage.MigrationOptions{})
		if err != nil {
			panic(err)
		}
	}

	categoryRepo := category.NewCategoryRepository(db)
	productRepo := product.NewProductRepository(db)
	reviewRepo := review.NewReviewRepository(db)
	productImageRepo := productImage.NewProductImageRepository(db)
	productSvc := service.NewProductService(
		categoryRepo,
		productRepo,
		reviewRepo,
		productImageRepo,
	)

	h := apiv1.NewCommerceServiceServer(twirpHandler.NewTwirpHandler(productSvc))

	mainServer := commonHTTP.NewEcho()
	mainServer.GET("/health", func(c echo.Context) error {
		return c.String(http.StatusOK, "ok")
	})
	mainServer.Any("/twirp/*", echo.WrapHandler(h))

	g, _ := errgroup.WithContext(ctx)

	g.Go(func() error {
		logrus.Info("Starting web server...")
		return mainServer.Start(":8080")
	})

	err = g.Wait()
	if err != nil {
		panic(err)
	}

	timeoutCtx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = mainServer.Shutdown(timeoutCtx)
	if err != nil {
		logrus.Fatal("Error when shutting down", err)
	}
}
