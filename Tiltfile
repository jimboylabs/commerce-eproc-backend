load('ext://dotenv', 'dotenv')
load('ext://restart_process', 'docker_build_with_restart')
load('ext://uibutton', 'cmd_button', 'location')

dotenv()

docker_compose('./docker-compose.yml')

compile_cmd_main = "CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o main ./cmd/..."

local_resource(
    "be-commerce-compile",
    compile_cmd_main,
    dir=os.environ["APP_CODE_PATH_HOST"],
    deps="**/*.go",
    labels=["backend"],
    trigger_mode=TRIGGER_MODE_MANUAL,
    auto_init=False,
)

commerce_vars = {k.lower(): v for k, v in os.environ.items() if k.startswith('COMMERCE_')}

docker_build(
    "be-commerce",
    context=os.environ["APP_CODE_PATH_HOST"],
    dockerfile="./Dockerfile",
    only=["./main", "./storage/migrations"],
    build_args=commerce_vars,
)

k8s_yaml("k8s/k8s.yml")

k8s_resource(
    "be-commerce",
    labels=["backend"],
    resource_deps=["be-commerce-compile"],
    port_forwards=["9000:8080"],
)
