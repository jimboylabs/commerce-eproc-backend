package service

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/spf13/cast"
	"gitlab.com/jimboylabs/commerce/common/pagination"
	"gitlab.com/jimboylabs/commerce/common/slice"
	"gitlab.com/jimboylabs/commerce/dto"
	"gitlab.com/jimboylabs/commerce/repository/category"
	"gitlab.com/jimboylabs/commerce/repository/product"
	"gitlab.com/jimboylabs/commerce/repository/productImage"
	"gitlab.com/jimboylabs/commerce/repository/review"
)

var _ ProductService = (*Product)(nil)

type ProductService interface {
	CreateCategory(ctx context.Context, name string) (categoryID string, err error)
	ListCategories(ctx context.Context) (result []*dto.CategoryDto, err error)
	CreateProduct(ctx context.Context, req dto.CreateProductDto) (productID string, err error)
	FindProductByID(ctx context.Context, productID *uuid.UUID) (product *dto.ProductDto, err error)
	UpdateProduct(ctx context.Context, req dto.UpdateProductDto) (result *dto.ProductDto, err error)
	ListProducts(ctx context.Context, req dto.ListProductsFilterDto, pageParam pagination.PageParam, sortBy, sortDirection string) (result *dto.ListProductsDto, err error)
	CreateReview(ctx context.Context, req dto.CreateReviewDto) (reviewID string, err error)
	CreateProductImage(ctx context.Context, req dto.CreateProductImageDto) (imageID string, err error)
}

type Product struct {
	categoryRepository     category.Repository
	productRepository      product.Repository
	reviewRepository       review.Repository
	productImageRepository productImage.Repository
}

func NewProductService(categoryRepository category.Repository, productRepository product.Repository, reviewRepository review.Repository, productImageRepository productImage.Repository) ProductService {
	return &Product{
		categoryRepository:     categoryRepository,
		productRepository:      productRepository,
		reviewRepository:       reviewRepository,
		productImageRepository: productImageRepository,
	}
}

// CreateCategory implements ProductService.
func (svc *Product) CreateCategory(ctx context.Context, name string) (categoryID string, err error) {
	category, err := svc.categoryRepository.Create(ctx, name)
	if err != nil {
		return categoryID, err
	}

	categoryID = category.ID

	return
}

// ListCategories implements ProductService.
func (svc *Product) ListCategories(ctx context.Context) (result []*dto.CategoryDto, err error) {
	categoriesMap, err := svc.categoryRepository.FindByIDs(ctx, []string{})
	if err != nil {
		return nil, err
	}

	result = make([]*dto.CategoryDto, 0)
	for _, c := range categoriesMap {
		result = append(result, &dto.CategoryDto{
			ID:   c.ID,
			Name: c.Name,
		})
	}

	return
}

// Create implements ProductService.
func (svc *Product) CreateProduct(ctx context.Context, req dto.CreateProductDto) (productID string, err error) {
	// create category
	category, err := svc.categoryRepository.FindByID(ctx, req.CategoryID)
	if err != nil {
		return productID, err
	}

	product := &product.Product{
		SKU:         req.SKU,
		Title:       req.Title,
		Description: req.Description,
		Price:       sql.NullInt64{Int64: req.Price, Valid: true},
		Currency:    req.Currency,
		CategoryID:  category.ID,
	}

	err = svc.productRepository.Create(ctx, product)
	if err != nil {
		return productID, err
	}

	productID = product.ID

	return
}

// FindProductByID implements ProductService.
func (svc *Product) FindProductByID(ctx context.Context, productID *uuid.UUID) (product *dto.ProductDto, err error) {
	productDB, err := svc.productRepository.FindByID(ctx, productID.String())
	if err != nil {
		return product, err
	}

	categoryDB, err := svc.categoryRepository.FindByID(ctx, productDB.CategoryID)
	if err != nil {
		return product, err
	}

	return &dto.ProductDto{
		ID:          productDB.ID,
		Title:       productDB.Title,
		SKU:         productDB.SKU,
		Description: productDB.Description,
		Price:       fmt.Sprintf("%d", productDB.Price.Int64),
		Currency:    productDB.Currency,
		Category: dto.CategoryDto{
			ID:   categoryDB.ID,
			Name: categoryDB.Name,
		},
	}, nil
}

// UpdateProduct implements ProductService.
func (svc *Product) UpdateProduct(ctx context.Context, req dto.UpdateProductDto) (result *dto.ProductDto, err error) {
	productDB, err := svc.productRepository.FindByID(ctx, req.ID)
	if err != nil {
		return result, err
	}

	if req.SKU != "" {
		productDB.SKU = req.SKU
	}

	if req.Title != "" {
		productDB.Title = req.Title
	}

	if req.Description != "" {
		productDB.Description = req.Description
	}

	if req.CategoryID != "" {
		productDB.CategoryID = req.CategoryID
	}

	err = svc.productRepository.Update(ctx, productDB)
	if err != nil {
		return result, err
	}

	categoryDB, err := svc.categoryRepository.FindByID(ctx, productDB.CategoryID)
	if err != nil {
		return result, err
	}

	return &dto.ProductDto{
		ID:          productDB.ID,
		Title:       productDB.Title,
		SKU:         productDB.SKU,
		Description: productDB.Description,
		Price:       fmt.Sprintf("%d", productDB.Price.Int64),
		Currency:    productDB.Currency,
		Category: dto.CategoryDto{
			ID:   categoryDB.ID,
			Name: categoryDB.Name,
		},
	}, nil
}

// ListProducts implements ProductService.
func (svc *Product) ListProducts(ctx context.Context, req dto.ListProductsFilterDto, pageParam pagination.PageParam, sortBy, sortDirection string) (result *dto.ListProductsDto, err error) {
	productsDB, err := svc.productRepository.List(ctx, product.ListProductsFilter{
		SKU:        req.SKU,
		Title:      req.Title,
		CategoryID: req.CategoryID,
	}, pageParam, sortBy, sortDirection)
	if err != nil {
		return nil, err
	}

	productIDs := make([]string, 0)
	categoryIDs := make([]string, 0)
	products := make([]*dto.ProductDto, 0)

	for _, p := range productsDB {
		products = append(products, &dto.ProductDto{
			ID:          p.ID,
			SKU:         p.SKU,
			Title:       p.Title,
			Description: p.Description,
			Price:       cast.ToString(p.Price.Int64),
			Currency:    p.Currency,
			Category: dto.CategoryDto{
				ID: p.CategoryID,
			},
			Rating: p.Rating,
		})
		productIDs = append(productIDs, p.ID)
		categoryIDs = slice.AppendUniqueString(categoryIDs, p.CategoryID)
	}

	categories, err := svc.categoryRepository.FindByIDs(ctx, categoryIDs)
	if err != nil {
		return nil, err
	}

	images, err := svc.productImageRepository.FindByProductIDs(ctx, productIDs)
	if err != nil {
		return nil, err
	}

	for i, p := range products {
		// Populate product category
		category, ok := categories[p.Category.ID]
		if ok {
			products[i].Category.Name = category.Name
		}

		// Populate product images
		imagesDB, ok := images[p.ID]
		if !ok {
			products[i].Images = []dto.ProductImageDto{}
			continue
		}

		for _, m := range imagesDB {
			products[i].Images = append(products[i].Images, dto.ProductImageDto{
				ID:        m.ID,
				ImageURL:  m.ImageURL,
				ShortDesc: m.ShortDesc,
			})
		}
	}

	paginated := pagination.Paginate(products, pageParam)

	return &dto.ListProductsDto{
		Products: paginated.Items.([]*dto.ProductDto),
		Page:     paginated.Page,
		PageSize: paginated.PageSize,
		HasNext:  paginated.HasNext,
		HasPrev:  paginated.HasPrev,
	}, nil
}

// CreateReview implements ProductService.
func (svc *Product) CreateReview(ctx context.Context, req dto.CreateReviewDto) (reviewID string, err error) {
	productDB, err := svc.productRepository.FindByID(ctx, req.ProductID)
	if err != nil {
		return reviewID, err
	}

	payload := &review.Review{
		ProductID: req.ProductID,
		Message:   req.Message,
		Rating:    req.Rating,
	}
	err = svc.reviewRepository.Create(ctx, payload)
	if err != nil {
		return reviewID, err
	}

	productDB.Rating = product.CalcNewRating(*productDB, req.Rating)

	err = svc.productRepository.Update(ctx, productDB)
	if err != nil {
		return reviewID, err
	}

	reviewID = payload.ID

	return
}

// CreateProductImage implements ProductService.
func (svc *Product) CreateProductImage(ctx context.Context, req dto.CreateProductImageDto) (imageID string, err error) {
	img := &productImage.ProductImage{
		ProductID: req.ProductID,
		ImageURL:  req.ImageURL,
		ShortDesc: req.ShortDesc,
	}

	err = svc.productImageRepository.Create(ctx, img)
	if err != nil {
		return imageID, err
	}

	imageID = img.ID

	return imageID, nil
}
