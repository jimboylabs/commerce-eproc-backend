package dto

type CreateProductDto struct {
	SKU         string
	Title       string
	Description string
	CategoryID  string
	Price       int64
	Currency    string
}

type UpdateProductDto struct {
	ID          string
	SKU         string
	Title       string
	Description string
	CategoryID  string
}

type CategoryDto struct {
	ID   string
	Name string
}

type ProductDto struct {
	ID          string
	SKU         string
	Title       string
	Description string
	Price       string
	Currency    string
	Rating      uint

	Category CategoryDto

	Images []ProductImageDto
}

type ListProductsFilterDto struct {
	SKU        string
	Title      string
	CategoryID string
}

type ListProductsDto struct {
	Products []*ProductDto

	Page     uint64
	PageSize uint64

	HasNext bool
	HasPrev bool
}

type CreateReviewDto struct {
	ProductID string
	Message   string
	Rating    uint
}

type CreateProductImageDto struct {
	ProductID string
	ImageURL  string
	ShortDesc string
}

type ProductImageDto struct {
	ID        string
	ImageURL  string
	ShortDesc string
}
