# Commerce Service

Commerce backend build for Eproc.tech technical assignment.

## Installation

### Before you start

You will need to have the following installed or configured, before proceeding:

- [Docker](https://www.docker.com/get-started/) and [Docker compose](https://docs.docker.com/compose/gettingstarted/)
- [Tilt.dev](https://docs.tilt.dev/)
- [Go](https://go.dev/dl/)
- [k3d](https://k3d.io/stable/)
- [kubectl](https://kubernetes.io/docs/tasks/tools/#kubectl)
- [Taskfile](https://taskfile.dev/)

### Running local kubernetes cluster

1. Create the cluster using the given config `k3d cluster create --config commerce-cluster.yml`
2. Command above will create and configure local image registry for the cluster
3. Ensure cluster is running by execute this command `kubectl cluster-info`

### Running the project

1. Clone the project to home directory `git clone git@gitlab.com:jimboylabs/commerce-eproc-backend.git ~/clones/commerce-eproc-backend/`
1. Create `.env` file by copy the example configuration `cp .env.example .env`
1. Run `tilt up`
1. Web server and DB migration will automatically running.
1. API server will be exposed to `http://localhost:9000`

### Testing the API

1. API docs was documented in the [docs](docs/api/v1) directory
1. Otherwise you can use [Bruno](https://github.com/usebruno/bruno/) and import collection provided in [bruno](bruno/) directory

### Generate Protobuf Go SDK

1. Task related was managed using Taskfile
1. Every changes to `.proto` files should be followed by generate the go sdk
1. Generate go sdk by executing `task install && task update && task gen`

### Running tests

1. Execute `task tests`

### Screenshots

<details>
  <summary>Tilt Dashboard</summary>
  ![Tilt Dashboard](screenshots/tilt-dashboard.png)
</details>

<details>
  <summary>Create Product</summary>
  ![Create Product](screenshots/create-product.png)
</details>

<details>
  <summary>List Products</summary>
  ![List Products](screenshots/list-products.png)
</details>
