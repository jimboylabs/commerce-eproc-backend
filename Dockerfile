FROM alpine:3.18.2

ARG commerce_database_url=postgres://postgres:postgres@host.k3d.internal:5432/commerce?sslmode=disable

ENV DATABASE_URL=$commerce_database_url
ENV DBMATE_MIGRATIONS_DIR=/migrations
ENV DBMATE_NO_DUMP_SCHEMA=true

RUN apk add --no-cache ca-certificates curl zip wget tzdata

COPY --from=ghcr.io/amacneil/dbmate /usr/local/bin/dbmate /usr/local/bin/dbmate

COPY main main
COPY storage/migrations /migrations

EXPOSE 8080 8081

ENTRYPOINT [ "/main" ]
