module gitlab.com/jimboylabs/commerce

go 1.21.1

require (
	github.com/Masterminds/squirrel v1.5.4
	github.com/amacneil/dbmate/v2 v2.7.0
	github.com/google/uuid v1.4.0
	github.com/labstack/echo/v4 v4.11.2
	github.com/lithammer/shortuuid/v3 v3.0.7
	github.com/mwitkow/go-proto-validators v0.3.2
	github.com/sethvargo/go-envconfig v0.9.0
	github.com/sirupsen/logrus v1.9.3
	github.com/spf13/cast v1.5.1
	github.com/twitchtv/twirp v8.1.3+incompatible
	github.com/xo/dburl v0.17.0
	golang.org/x/sync v0.5.0
	google.golang.org/protobuf v1.31.0
)

require (
	github.com/gogo/protobuf v1.3.0 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/labstack/gommon v0.4.0 // indirect
	github.com/lann/builder v0.0.0-20180802200727-47ae307949d0 // indirect
	github.com/lann/ps v0.0.0-20150810152359-62de8c46ede0 // indirect
	github.com/lib/pq v1.10.9 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	golang.org/x/crypto v0.14.0 // indirect
	golang.org/x/net v0.17.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	golang.org/x/time v0.3.0 // indirect
)
